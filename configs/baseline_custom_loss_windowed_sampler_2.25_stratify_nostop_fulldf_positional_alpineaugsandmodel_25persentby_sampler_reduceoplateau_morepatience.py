import os

from rsna.dataset import get_weghted_sampler
from rsna.gen_mask import Positional_encoding

import torch
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.optim import Adam
import pandas as pd
from albumentations import (
    CLAHE,
    RandomRotate90,
    Transpose,
    ShiftScaleRotate,
    Blur,
    OpticalDistortion,
    GridDistortion,
    Compose,
    Resize,
    Normalize,
    HorizontalFlip,
    RandomBrightnessContrast,
    RandomResizedCrop,
    VerticalFlip,
    Rotate
)
from albumentations.pytorch import ToTensor
from pytorch_lightning.logging import TestTubeLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from iterstrat.ml_stratifiers import MultilabelStratifiedKFold


name = "baseline_custom_loss_windowed_sampler_2.25_stratify_nostop_fulldf_positional_alpineaugsandmodel_25persentby_sampler_reduceoplateau_morepatience"

df = pd.read_csv("/src/workspace/csvs/train_png_512.csv")
X, y = df.iloc[:, 0], df.iloc[:, 1:]
mskf = MultilabelStratifiedKFold(n_splits=5, random_state=0)

for train_index, test_index in mskf.split(X, y):
    train, val = df.iloc[train_index], df.iloc[test_index]
    break

# train, val = train_test_split(df, test_size=0.2, random_state=0)
test_df = pd.read_csv("/src/workspace/csvs/test_png_512.csv")
train_batch_size = 12

model = torch.hub.load("facebookresearch/WSL-Images", "resnext101_32x8d_wsl")
model.fc = torch.nn.Linear(2048, 6)
"""model.avg_pool = nn.AdaptiveAvgPool2d(1)
    model.last_linear = nn.Linear(
        model.last_linear.in_features,
        cfg.model.n_output,
    )
"""
model.weights = torch.Tensor([1.0, 1.0, 1.0, 1.0, 1.0, 2.0])

pos_encoder = Positional_encoding(512, 512)

augs = Compose(
                    [
                        RandomResizedCrop(512, 512, scale=(0.7, 1), p=1),
                        # Resize(512, 512),
                        HorizontalFlip(p=0.5),
                        # VerticalFlip(p=0.5),
                        Rotate(limit=30, border_mode=0, p=0.7),
                        RandomBrightnessContrast(brightness_limit=0.08, contrast_limit=0.08, p=0.5),
                        ToTensor(),
                        
                    ]
                )

config = {
    "name": name,
    "backbone": model,
    "stages_schedule":{0: 0}, 
    "stages": [
        # 0 stage
        {
            "train_df": {
                "transform": augs,
                "df": train,
                "windowing": True,
                "positional_mask": pos_encoder,
            },
            "val_df": {
                "transform": augs,
                "df": val,
                "windowing": True,
                "positional_mask": pos_encoder,
            },
            "test_df": {
                "transform": augs,
                "df": test_df,
                "windowing": True,
                "positional_mask": pos_encoder,
            },
            "train_batch_size": train_batch_size,
            "val_batch_size": int(train_batch_size * 2.2),
            "test_batch_size": train_batch_size * 6,

            "train_sampler": get_weghted_sampler(train, 2.25, 1, 0.25),
            "lr": 6e-5,
            "optimizer": {"base_opter": Adam},
            "scheduler": {"base_scheduler": ReduceLROnPlateau, "patience": 2, "factor": 0.3}, 
        }
    ],
    "trainer": {
        "max_nb_epochs": 20,
        "use_amp": True,
        "gpus": [0],
        # "distributed_backend": "dp",
        # "train_percent_check": 1,
        # "log_gpu_memory": "all",
        # "fast_dev_run": True,
        "checkpoint_callback": ModelCheckpoint(
            filepath=os.path.join("/src/workspace/models", name),
            save_best_only=True,
            verbose=True,
            monitor="val_loss",
            mode="min",
            prefix="",
        ),
        "early_stop_callback": EarlyStopping(
            monitor="val_loss", min_delta=0.00, patience=6, verbose=True, mode="min"
        ),
        "logger": TestTubeLogger(os.path.join("/src/workspace/exp", name)),
    },
}


config["trainer"]["logger"].experiment.tag(config)

