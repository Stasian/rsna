import os

from rsna.dataset import get_weghted_sampler
from rsna.gen_mask import Positional_encoding

import torch
import pandas as pd
from albumentations import (
    CLAHE,
    RandomRotate90,
    Transpose,
    ShiftScaleRotate,
    Blur,
    OpticalDistortion,
    GridDistortion,
    Compose,
    Resize,
    Normalize,
    HorizontalFlip,
    RandomBrightnessContrast,
)
from albumentations.pytorch import ToTensor
from pytorch_lightning.logging import TestTubeLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from iterstrat.ml_stratifiers import MultilabelStratifiedKFold


name = "baseline_custom_loss_windowed_sampler_2_stratify_nostop_fulldf_basicaug_nonormalization_positional"

df = pd.read_csv("/src/workspace/csvs/train_png.csv")
X, y = df.iloc[:, 0], df.iloc[:, 1:]
mskf = MultilabelStratifiedKFold(n_splits=5, random_state=0)

for train_index, test_index in mskf.split(X, y):
    train, val = df.iloc[train_index], df.iloc[test_index]
    break

# train, val = train_test_split(df, test_size=0.2, random_state=0)
test_df = pd.read_csv("/src/workspace/csvs/test_png.csv")
train_batch_size = 50

model = torch.hub.load("facebookresearch/WSL-Images", "resnext101_32x8d_wsl")
model.fc = torch.nn.Linear(2048, 6)
model.weights = torch.Tensor([1.0, 1.0, 1.0, 1.0, 1.0, 2.0])

pos_encoder = Positional_encoding(224, 224)

config = {
    "name": name,
    "backbone": model,
    "stages": [
        # 0 stage
        {
            "train_df": {
                "transform": Compose(
                    [
                        HorizontalFlip(),
                        RandomBrightnessContrast(),
                        ShiftScaleRotate(),
                        ToTensor(),
                    ]
                ),
                "df": train,
                "windowing": True,
                "positional_mask": pos_encoder,
            },
            "val_df": {
                "transform": Compose(
                    [
                        HorizontalFlip(),
                        RandomBrightnessContrast(),
                        ShiftScaleRotate(),
                        ToTensor(),
                    ]
                ),
                "df": val,
                "windowing": True,
                "positional_mask": pos_encoder,
            },
            "test_df": {
                "transform": Compose(
                    [
                        HorizontalFlip(),
                        RandomBrightnessContrast(),
                        ShiftScaleRotate(),
                        ToTensor(),
                    ]
                ),
                "df": test_df,
                "windowing": True,
                "positional_mask": pos_encoder,
            },
            "train_batch_size": train_batch_size,
            "val_batch_size": int(train_batch_size * 2.5),
            "test_batch_size": train_batch_size * 7,
            "lr": 2e-5,
            "train_sampler": get_weghted_sampler(train, 2, 1),
        }
    ],
    "trainer": {
        "max_nb_epochs": 20,
        "use_amp": True,
        "gpus": [0],
        # "distributed_backend": "dp",
        # "train_percent_check": 0.05,
        "log_gpu_memory": "all",
        # "fast_dev_run": True,
        "checkpoint_callback": ModelCheckpoint(
            filepath=os.path.join("/src/workspace/models", name),
            save_best_only=True,
            verbose=True,
            monitor="val_loss",
            mode="min",
            prefix="",
        ),
        "early_stop_callback": EarlyStopping(
            monitor="val_loss", min_delta=0.00, patience=1, verbose=True, mode="min"
        ),
        "logger": TestTubeLogger(os.path.join("/src/workspace/exp", name)),
    },
}


config["trainer"]["logger"].experiment.tag(config)

