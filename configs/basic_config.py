import os

from sklearn.model_selection import train_test_split
from test_tube import Experiment
import pandas as pd
from albumentations import Compose, ShiftScaleRotate, Resize, Normalize
from albumentations.pytorch import ToTensor
from pytorch_lightning.logging import TestTubeLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping

weights = [1.0, 1.0, 1.0, 1.0, 1.0, 2.0]


def my_loss(y_pred, y_true):
    return F.binary_cross_entropy_with_logits(
        y_pred, y_true, weights.repeat(y_pred.shape[0], 1)
    )


name = "baseline"

df = pd.read_csv("/src/workspace/csvs/train.csv")
train, val = train_test_split(df, test_size=0.2, random_state=0)
test_df = pd.read_csv("/src/workspace/csvs/test.csv")

config = {
    "name": name,
    "stages": [
        # 0 stage
        {
            "train_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        ShiftScaleRotate(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ]
                ),
                "df": train,
                ##TODO normal sklearn split
            },
            "val_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        ShiftScaleRotate(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ]
                ),
                "df": val,
            },
            "test_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        ShiftScaleRotate(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ]
                ),
                "df": test_df,
            },
            "train_batch_size": 60,
            "val_batch_size": 570,
            "test_batch_size": 570,
        }
    ],
    "trainer": {
        "max_nb_epochs": 10,
        "use_amp": True,
        "gpus": [0],
        "train_percent_check": 0.05,
        "log_gpu_memory": "all",
        # "default_save_path": os.path.join("/src/workspace/models", name),
        # "fast_dev_run": True,
        "checkpoint_callback": ModelCheckpoint(
            filepath=os.path.join("/src/workspace/models", name),
            save_best_only=True,
            verbose=True,
            monitor="val_loss",
            mode="min",
            prefix="",
        ),
        "early_stop_callback": EarlyStopping(
            monitor="val_loss", min_delta=0.00, patience=1, verbose=False, mode="min"
        ),
        "logger": TestTubeLogger(os.path.join("/src/workspace/exp", name)),
    },
}


config["trainer"]["logger"].experiment.tag(config)
