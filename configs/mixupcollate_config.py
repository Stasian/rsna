import os

from rsna.model import CoolModel
from rsna.dataset import get_weghted_sampler
from rsna.data_utils import Mixup_collate

from sklearn.model_selection import train_test_split
import pandas as pd
from albumentations import (
    CLAHE,
    RandomRotate90,
    Transpose,
    ShiftScaleRotate,
    Blur,
    OpticalDistortion,
    GridDistortion,
    Compose,
    Resize,
    Normalize,
)
from albumentations.pytorch import ToTensor
from pytorch_lightning.logging import TestTubeLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping


name = "baseline_custom_loss_windowed_sampler_dataloaderfix2.5,1"

df = pd.read_csv("/src/workspace/csvs/train.csv")
train, val = train_test_split(df, test_size=0.2, random_state=0)
test_df = pd.read_csv("/src/workspace/csvs/test.csv")


config = {
    "name": name,
    "model": CoolModel(),
    "stages": [
        # 0 stage
        {
            "train_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        ShiftScaleRotate(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ]
                ),
                "df": train,
                "windowing": True,
                # TODO normal sklearn split
            },
            "val_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        ShiftScaleRotate(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ]
                ),
                "df": val,
                "windowing": True,
            },
            "test_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        ShiftScaleRotate(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ]
                ),
                "df": test_df,
                "windowing": True,
            },
            "train_batch_size": 70,
            "val_batch_size": 125,
            "test_batch_size": 400,
            "lr": 2e-5,
            "train_sampler": get_weghted_sampler(train, 2.5, 1),
            # "collate_function": Mixup_collate({1: 30, 2: 20}),
        }
    ],
    "trainer": {
        "max_nb_epochs": 20,
        "use_amp": True,
        "gpus": [0],
        "train_percent_check": 0.05,
        "log_gpu_memory": "all",
        # "default_save_path": os.path.join("/src/workspace/models", name),
        # "fast_dev_run": True,
        "checkpoint_callback": ModelCheckpoint(
            filepath=os.path.join("/src/workspace/models", name),
            save_best_only=True,
            verbose=True,
            monitor="val_loss",
            mode="min",
            prefix="",
        ),
        "early_stop_callback": EarlyStopping(
            monitor="val_loss", min_delta=0.00, patience=4, verbose=True, mode="min"
        ),
        "logger": TestTubeLogger(os.path.join("/src/workspace/exp", name)),
    },
}


config["trainer"]["logger"].experiment.tag(config)
