import argparse
import os
from importlib import import_module

from rsna.dataset_processing import create_submission
from rsna.model import CoolModel

import torch
from pytorch_lightning import Trainer

parser = argparse.ArgumentParser(description="run config")
parser.add_argument("--config", type=str, help="path to module")
parser.add_argument("--send", action="store_true", help="submit?")
parser.add_argument("--fit", action="store_true", help="fit or only predict existed?")
parser.add_argument("--predict", action="store_true", help="predict?")
parser.add_argument("--tta", type=int, default=1, help="predict?")


args = parser.parse_args()
config_module = import_module(args.config)
config = config_module.config
os.system(f"cp /src/workspace/src/{args.config}.py /src/workspace/configs/{config['name']}.py")  # save config
submission_path = f"/src/workspace/csvs/{config['name']}_submission.csv"

model = CoolModel(config)

# model.setup_stage(config["stages"][0])
trainer = Trainer(**config["trainer"])
if args.fit:
    trainer.fit(model)
if args.predict:
    test_pred = None
    for _ in range(args.tta):
        trainer.test(model)
        pred = model.all_preds
        if test_pred is None:
            test_pred = pred
        else:
            test_pred += pred
        model.all_preds = None  #
    test_pred = torch.sigmoid(test_pred / args.tta).numpy()

    create_submission(submission_path, test_pred)

if args.send:
    os.system(
        f"kaggle competitions submit -c rsna-intracranial-hemorrhage-detection -f {submission_path} -m {config_module.config['name']}_tta={args.tta}"
    )
