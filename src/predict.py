import argparse
import os
from importlib import import_module

from rsna.dataset_processing import create_submission

import torch
from pytorch_lightning import Trainer

parser = argparse.ArgumentParser(description="run config")
parser.add_argument("--config", type=str, help="path to module")
parser.add_argument(
    "--tta", type=int, default=2, help="how many tta we need to apply"
)
parser.add_argument("--send", action="store_true", help="submit?")

args = parser.parse_args()
config_module = import_module(args.config)
config = config_module.config
submission_path = f"/src/workspace/csvs/{config_module.config['name']}_submission.csv"
preds = None

for weights_path, tags_csv in config["models_list"]:
    model = config["model"]
    model.setup_stage(config["stages"][0])
    model.load_from_metrics(weights_path=weights_path, tags_csv=tags_csv)
    trainer = Trainer(**config["trainer"])  # recheck setup
    for _ in range(args.tta):
        trainer.test(model)
        pred = model.all_preds
        if preds is None:
            preds = pred
        else:
            preds += pred

test_pred = torch.sigmoid(preds / (len(config["models_list"]) * args.tta)).numpy()
create_submission(submission_path, test_pred)

if args.send:
    os.system(
        f"kaggle competitions submit -c rsna-intracranial-hemorrhage-detection -f {submission_path} -m {config_module.config['name']}"
    )
