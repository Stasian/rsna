import os

from rsna.model import CoolModel

import pandas as pd
from sklearn.model_selection import train_test_split
from albumentations import (
    CLAHE,
    RandomRotate90,
    Transpose,
    ShiftScaleRotate,
    Blur,
    OpticalDistortion,
    GridDistortion,
    Compose,
    Resize,
    Normalize,
)
from albumentations.pytorch import ToTensor
from pytorch_lightning.logging import TestTubeLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping


name = "baseline_custom_loss_windowed_sampler_dataloaderfix_littleaugs"

df = pd.read_csv("/src/workspace/csvs/train.csv")
train, val = train_test_split(df, test_size=0.2, random_state=0)
test_df = pd.read_csv("/src/workspace/csvs/test.csv")

config = {
    "name": name,
    "model": CoolModel(),
    "models_list": [
        [
            "/src/workspace/models/baseline_custom_loss_windowed_sampler_dataloaderfix_littleaugs/_ckpt_epoch_3.ckpt",
            "/src/workspace/exp/baseline_custom_loss_windowed_sampler_dataloaderfix_littleaugs/default/version_7/meta_tags.csv",
        ]
    ],
    "stages": [
        # 0 stage
        {
            "train_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        CLAHE(),
                        RandomRotate90(),
                        Transpose(),
                        ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.50, rotate_limit=45, p=.75),
                        Blur(blur_limit=3),
                        OpticalDistortion(),
                        GridDistortion(),
                        # HueSaturationValue(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ], p=1
                ),
                "df": train,
                "windowing": True,
                # TODO normal sklearn split
            },
            "val_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        CLAHE(),
                        RandomRotate90(),
                        Transpose(),
                        ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.50, rotate_limit=45, p=.75),
                        Blur(blur_limit=3),
                        OpticalDistortion(),
                        GridDistortion(),
                        # HueSaturationValue(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ], p=1
                ),
                "df": val,
                "windowing": True,
            },
            "test_df": {
                "transform": Compose(
                    [
                        Resize(224, 224),
                        CLAHE(),
                        RandomRotate90(),
                        Transpose(),
                        ShiftScaleRotate(
                            shift_limit=0.0625,
                            scale_limit=0.50,
                            rotate_limit=45,
                            p=0.75,
                        ),
                        Blur(blur_limit=3),
                        OpticalDistortion(),
                        GridDistortion(),
                        # HueSaturationValue(),
                        Normalize(mean=[0.485], std=[0.229]),
                        ToTensor(),
                    ],
                    p=1,
                ),
                "df": test_df,
                "windowing": True,
            },
            "train_batch_size": 50,
            "val_batch_size": int(50 * 2.5),
            "test_batch_size": 50 * 8,
            "lr": 2e-5,
        }
    ],
    "trainer": {
        "max_nb_epochs": 20,
        "use_amp": True,
        "gpus": [0],
        "train_percent_check": 0.05,
        "log_gpu_memory": "all",
        # "default_save_path": os.path.join("/src/workspace/models", name),
        # "fast_dev_run": True,
        "checkpoint_callback": ModelCheckpoint(
            filepath=os.path.join("/src/workspace/models", name),
            save_best_only=True,
            verbose=True,
            monitor="val_loss",
            mode="min",
            prefix="",
        ),
        "early_stop_callback": EarlyStopping(
            monitor="val_loss", min_delta=0.00, patience=1, verbose=False, mode="min"
        ),
        "logger": TestTubeLogger(os.path.join("/src/workspace/exp", name)),
    },
}

config["trainer"]["logger"].experiment.tag(config)
