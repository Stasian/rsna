import os
import glob

from data_utils import read_dcm

import cv2
from joblib import Parallel, delayed
from albumentations import Resize


rs = Resize(512, 512)


def convert_to_png(dcm_in, dir_img):
    try:
        img = read_dcm(dcm_in, windowed=True)
    except ValueError as exp:
        print(exp)
        return 0
    m = rs.apply(img)
    cv2.imwrite(os.path.join(dir_img, os.path.basename(dcm_in)[:-3] + "png"), m)


# Extract images in parallel
if __name__ == "__main__":

    dir_dcm = "/data/data/rsna/stage_1_train_images"
    dir_img = "/data/data/rsna/stage_1_train_images_png_512"

    dicom = glob.glob(os.path.join(dir_dcm, "*.dcm"))
    print(len(dicom))
    Parallel(n_jobs=64, verbose=2)(delayed(convert_to_png)(i, dir_img) for i in dicom)

    dir_dcm = "/data/data/rsna/stage_1_test_images"
    dir_img = "/data/data/rsna/stage_1_test_images_png_512"

    dicom = glob.glob(os.path.join(dir_dcm, "*.dcm"))
    print(len(dicom))
    Parallel(n_jobs=64, verbose=2)(delayed(convert_to_png)(i, dir_img) for i in dicom)

