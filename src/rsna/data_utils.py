import pydicom
import numpy as np
import torch


def read_dcm(img_name, windowed=True):
    dcm = pydicom.dcmread(img_name)
    window_center, window_width, intercept, slope = get_windowing(dcm)

    img = pydicom.read_file(img_name).pixel_array
    if windowed:
        img = window_image(img, window_center, window_width, intercept, slope)
    return np.array(img).astype("uint8")


def window_image(img, window_center, window_width, intercept, slope):
    img = img * slope + intercept
    img_min = window_center - window_width // 2
    img_max = window_center + window_width // 2
    img[img < img_min] = img_min
    img[img > img_max] = img_max
    return img


def get_first_of_dicom_field_as_int(x):
    # get x[0] as in int is x is a 'pydicom.multival.MultiValue', otherwise get int(x)
    if type(x) == pydicom.multival.MultiValue:
        return int(x[0])
    else:
        return int(x)


def get_windowing(data):
    dicom_fields = [
        data[("0028", "1050")].value,  # window center
        data[("0028", "1051")].value,  # window width
        data[("0028", "1052")].value,  # intercept
        data[("0028", "1053")].value,
    ]  # slope
    return [get_first_of_dicom_field_as_int(x) for x in dicom_fields]


class Mixup:
    def __init__(self):
        self.cnt = 0
        self.img = None
        self.class_ = None

    def __call__(self, image, class_):
        if self.img is None:
            self.img = image
            self.class_ = class_
        else:
            self.img += image
            self.class_ += class_
        self.cnt += 1

    def get_res(self):
        im, cl = self.img / self.cnt, self.class_
        cl = torch.clamp(cl, 0, 1)
        self.cnt = 0
        self.img, self.class_ = None, None
        return im, cl


class Mixup_collate:
    def __init__(self, values={}):
        self.mixup = Mixup()
        self.values = values  # {1:20, 2:20}
        self.seq = self.generate_sequence()

    def generate_sequence(self):
        seq = []
        for key in self.values:
            for _ in range(self.values[key]):
                seq.append(key)
        return seq

    def __call__(self, batch):
        res = {"image": [], "labels": []}
        data = [i["image"] for i in batch]
        target = [i["labels"] for i in batch]
        i, j = 0, 0
        while i < len(batch):
            for _ in range(self.seq[j]):
                self.mixup(data[i], target[i])
                i += 1
            im, cl = self.mixup.get_res()
            res["image"].append(im)
            res["labels"].append(cl)
            j += 1
        res["image"] = torch.stack(res["image"])
        res["labels"] = torch.stack(res["labels"])
        return res
