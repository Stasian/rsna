import cv2
import numpy as np
import torch
from torch.utils.data import Dataset
from torch.utils.data.sampler import WeightedRandomSampler


class IntracranialDataset(Dataset):
    def __init__(
        self,
        data,
        labels,
        transform=None,
        windowing=True,
        png=True,
        positional_mask=None,
    ):

        self.data = data.reset_index()
        self.transform = transform
        self.labels = labels
        self.windowing = windowing
        self.read_img = self.read_png if png else self.read_dicom
        self.positional_mask = positional_mask

    @staticmethod
    def read_png(img):
        return cv2.imread(img)[:, :, 0]

    def __len__(self):

        return len(self.data)

    def __getitem__(self, idx):

        img_name = self.data.loc[idx, "Image"]
        img = self.read_img(img_name)

        if self.transform:
            augmented = self.transform(image=img)
            img = augmented["image"]

        img_ = torch.zeros((3, img.shape[0], img.shape[1]))  # фу блядь
        img_[0] = img
        if self.positional_mask is not None:
            img_[1] = torch.from_numpy(self.positional_mask())
        else:
            img_[1] = img
        img_[2] = img
        #

        if self.labels:

            labels = torch.tensor(
                self.data.loc[
                    idx,
                    [
                        "epidural",
                        "intraparenchymal",
                        "intraventricular",
                        "subarachnoid",
                        "subdural",
                        "any",
                    ],
                ]
            )
            return {"image": img_, "labels": labels.float()}

        else:

            return {"image": img_}


def get_weghted_sampler(train, any_weight, other_weight, len_factor=1):
    any_classes = train["any"] == 1
    notany = train["any"] == 0
    sampler_weigts = np.zeros(len(train))
    sampler_weigts[any_classes] = any_weight
    sampler_weigts[notany] = other_weight

    return WeightedRandomSampler(sampler_weigts, int(len(sampler_weigts) * len_factor))
