import os
import glob

import numpy as np
import pandas as pd


def create_submission(
    submission_path,
    test_pred,
    sample_csv_path="/data/data/rsna/stage_1_sample_submission.csv",
):
    submission = pd.read_csv(sample_csv_path)
    submission = pd.concat(
        [submission.drop(columns=["Label"]), pd.DataFrame(test_pred)], axis=1
    )
    submission.columns = ["ID", "Label"]
    submission.to_csv(submission_path, index=False)


if __name__ == "__main__":
    dir_csv = "/data/data/rsna"
    dir_train_img = "/data/data/rsna/stage_1_train_images_png_512"
    dir_test_img = "/data/data/rsna/stage_1_test_images_png_512"

    train = pd.read_csv(os.path.join(dir_csv, "stage_1_train.csv"))
    test = pd.read_csv(os.path.join(dir_csv, "stage_1_sample_submission.csv"))

    train[["ID", "Image", "Diagnosis"]] = train["ID"].str.split("_", expand=True)
    train = train[["Image", "Diagnosis", "Label"]]
    train.drop_duplicates(inplace=True)
    train = train.pivot(
        index="Image", columns="Diagnosis", values="Label"
    ).reset_index()
    train["Image"] = "ID_" + train["Image"]
    print(train.head())
    print(train.shape)

    """    undersample_seed = 0
    num_ill_patients = train[train["any"] == 1].shape[0]

    healthy_patients = train[train["any"] == 0].index.values
    healthy_patients_selection = np.random.RandomState(undersample_seed).choice(
        healthy_patients, size=num_ill_patients, replace=False
    )

    sick_patients = train[train["any"] == 1].index.values
    selected_patients = list(set(healthy_patients_selection).union(set(sick_patients)))
    new_train = train.loc[selected_patients].copy()"""

    dcm = glob.glob(os.path.join(dir_train_img, "*.png"))
    dcm = [os.path.basename(dcm)[:-4] for dcm in dcm]
    dcm = np.array(dcm)

    print(train.head())
    print(dcm)
    train = train[train["Image"].isin(dcm)]
    train["Image"] = train["Image"].apply(
        lambda x: os.path.join(dir_train_img, x) + ".png"
    )
    train.to_csv("/src/workspace/csvs/train_png_512.csv", index=False)
    print(train.shape)

    test[["ID", "Image", "Diagnosis"]] = test["ID"].str.split("_", expand=True)
    test["Image"] = "ID_" + test["Image"]
    test["Image"] = test["Image"].apply(
        lambda x: os.path.join(dir_test_img, x) + ".png"
    )
    test = test[["Image", "Label"]]
    test.drop_duplicates(inplace=True)

    test.to_csv("/src/workspace/csvs/test_png_512.csv", index=False)
