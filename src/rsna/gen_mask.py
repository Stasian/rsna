import numpy as np
from albumentations import Resize


class Positional_encoding:
    def __init__(self, height, width):
        self.height, self.width = height, width
        rs = Resize(height, width)
        self.mask = self.generate_mask()
        self.fill_mask()
        self.mask = rs.apply(self.mask)
        self.normalize()

    def __call__(self):
        return self.mask

    def generate_mask(self):
        return np.zeros((self.height * 4 + 1, self.width * 4 + 1))

    def fill_mask(self):
        center = np.array([self.mask.shape[0] // 2, self.mask.shape[1] // 2])
        for i in range(self.mask.shape[0]):
            for j in range(self.mask.shape[1]):
                self.mask[i, j] = np.sqrt((i - center[0]) ** 2 + (j - center[1]) ** 2)

    def normalize(self):
        max_ = self.mask.max()
        self.mask = self.mask / max_
        self.mask = 1 - self.mask
