from .dataset import IntracranialDataset

import torch
import torch.nn.functional as F
import pytorch_lightning as pl
from torch.optim import Adam



class BaseModel(pl.LightningModule):
    def __init__(self, config=None):
        super(BaseModel, self).__init__()

        if "backbone" in config:
            self.model = config["backbone"]

        self.setup_defaults()
        self.config = config
        
        self.setup_stage(self.config["stages"][0])  # TODO

    def setup_defaults(self):
        self.scheduler = None
        self.train_collate_function = None
        self.train_sampler = None  # TODO
        self.train_shuffle = True
        self.optimizer = None

#    def on_epoch_start(self):
#        print("kek")
#        epoch = self.trainer.epoch
#        stage_schedule = self.config["stages_schedule"]
#        if epoch in stage_schedule:
#            self.setup_stage(self.config["stages"][stage_schedule[epoch]])
#        print(self.lr)
#        1 / 0


    def setup_stage(self, stage):
        if "train_batch_size" in stage:  # TODO
            self.train_batch_size = stage["train_batch_size"]
        if "val_batch_size" in stage:
            self.val_batch_size = stage["val_batch_size"]
        if "test_batch_size" in stage:
            self.test_batch_size = stage["test_batch_size"]
        if "lr" in stage:
            self.lr = stage["lr"]
        if "loss" in stage:
            self.loss = stage["loss"]
        if "train_sampler" in stage:
            self.train_sampler = stage["train_sampler"]
            self.train_shuffle = False
        if "collate_function" in stage:
            self.train_collate_function = stage["collate_function"]
        if "optimizer" in stage:
            self.set_optimizer(**stage["optimizer"])
        if "scheduler" in stage:
            self.set_scheduler(**stage["scheduler"])

        if "train_df" in stage:
            self.set_train_dataset(**stage["train_df"])
        if "val_df" in stage:
            self.set_val_dataset(**stage["val_df"])
        if "test_df" in stage:
            self.set_test_dataset(**stage["test_df"])

    def set_scheduler(self, base_scheduler, patience=1, factor=0.1):
        if self.scheduler is None:
            self.scheduler = base_scheduler(self.optimizer, patience=patience, factor=factor)
        
    def set_optimizer(self, base_opter):
        if self.optimizer is None:
            self.optimizer = base_opter(self.parameters(), lr=self.lr)

    def set_train_dataset(self, df, transform, windowing=True, positional_mask=None):
        dataset = IntracranialDataset(
            data=df,
            transform=transform,
            labels=True,
            windowing=windowing,
            positional_mask=positional_mask,
        )
        self.train_dataset = dataset

    def set_val_dataset(self, df, transform, windowing=True, positional_mask=None):
        dataset = IntracranialDataset(
            data=df,
            transform=transform,
            labels=True,
            windowing=windowing,
            positional_mask=positional_mask,
        )
        self.val_dataset = dataset

    def set_test_dataset(self, df, transform, windowing=True, positional_mask=None):
        dataset = IntracranialDataset(
            data=df,
            transform=transform,
            labels=False,
            windowing=windowing,
            positional_mask=positional_mask,
        )
        self.test_dataset = dataset

    def forward(self, x):
        return self.model(x)

    def eval_loss(self, y_hat, y):
        loss = self.loss(y_hat, y)
        return loss

    def training_step(self, batch, batch_nb):
        x, y = batch["image"], batch["labels"]
        y_hat = self.forward(x)
        loss = self.eval_loss(y_hat, y)
        # tensorboard_logs = {"train_loss": loss}
        return {"loss": loss}

    def training_end(self, outputs):
        avg_loss = torch.stack([x["train_loss"] for x in outputs]).mean()
        self.logger.experiment.log({"avg_train_loss": avg_loss})
        return {"avg_train_loss": avg_loss}

    def validation_step(self, batch, batch_nb):
        x, y = batch["image"], batch["labels"]
        y_hat = self.forward(x)
        loss = self.eval_loss(y_hat, y)
        # self.logger.experiment.log({"val_loss": loss})
        # tensorboard_logs = {"val_loss": loss}
        return {"iter_val_loss": loss}

    def validation_end(self, outputs):
        avg_loss = torch.stack([x["iter_val_loss"] for x in outputs]).mean()
        if self.scheduler:
            self.scheduler.step(avg_loss)
        self.logger.experiment.log({"avg_val_loss": avg_loss})
        return {"val_loss": avg_loss}

    def test_step(self, batch, batch_nb):
        x = batch["image"]
        y_hat = self.forward(x)
        return {"test_pred": y_hat}

    def test_end(self, outputs):
        all_preds = None
        for x in outputs:
            if all_preds is None:
                all_preds = x["test_pred"]
            else:
                all_preds = torch.cat([all_preds, x["test_pred"]])
        self.all_preds = all_preds.detach().cpu().reshape(-1, 1)
        return {"all_preds": all_preds.mean()}

    def configure_optimizers(self):
        self.optimizer = Adam(self.parameters(), lr=self.lr)
        return self.optimizer

    @pl.data_loader
    def train_dataloader(self):
        dl = torch.utils.data.DataLoader(
            self.train_dataset,
            batch_size=self.train_batch_size,
            shuffle=self.train_shuffle,
            sampler=self.train_sampler,
            collate_fn=self.train_collate_function,
            num_workers=32,
            pin_memory=True,
        )
        return dl

    @pl.data_loader
    def val_dataloader(self):
        dl = torch.utils.data.DataLoader(
            self.val_dataset,
            batch_size=self.val_batch_size,
            shuffle=False,
            num_workers=32,
            pin_memory=True,
        )
        return dl

    @pl.data_loader
    def test_dataloader(self):
        return torch.utils.data.DataLoader(
            self.test_dataset,
            batch_size=self.test_batch_size,
            shuffle=False,
            num_workers=32,
        )


class CoolModel(BaseModel):
    def __init__(self, config=None):
        super(CoolModel, self).__init__(config=config)

    def eval_loss(self, y_pred, y_true):
        return F.binary_cross_entropy_with_logits(
            y_pred, y_true, self.model.weights.repeat(y_pred.shape[0], 1).cuda()
        )
